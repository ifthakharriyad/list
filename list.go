// Package list impliments linked list.
//
// It supports insert, search, predecessor, and delete operations
// on a list.
package list

// A Sint represents a node in a linked list.
// Here, Val is an integer value, and the Next points
// to the next node if exists, or otherwise to the nil pointer.
type Sint struct {
	Val  int
	Next *Sint
}

// SinglyInt initiates a singly integer linked list
// with an initial value. It returns the pointer
// to the first node.
func SinglyInt(v int) (p *Sint) {
	return &Sint{v, nil}
}

// Insert appends a node with the given value
// into the list.
func (l *Sint) Insert(v int) {
	if l.Next == nil {
		l.Next = &Sint{v, nil}
		return
	} else {
		l.Next.Insert(v)
	}
}

// Search look for the given value in the list.
// It returns the pointer to the value if
// found, or nil if doesn't.
func (l *Sint) Search(v int) *Sint {
	if l == nil {
		return nil
	}
	if (*l).Val == v {
		return l
	} else {
		return l.Next.Search(v)
	}

}

// Pred find the predecessor of the given value in
// the list. It returns the pointer to the value
// if found, or nil if doesn't.
func (l *Sint) Pred(v int) *Sint {
	if l == nil || l.Next == nil || l.Val == v {
		return nil
	}
	if (*l.Next).Val == v {
		return l
	} else {
		return l.Next.Pred(v)
	}
}

// Delete removes the node containing the given value
// from the list, if exists, of course.
func (l *Sint) Delete(v int) {
	p := l.Search(v)
	if p == nil {
		return
	}
	pred := l.Pred(v)
	if pred == nil {
		l.Val = l.Next.Val
		l.Next = l.Next.Next
		return
	}
	pred.Next = p.Next
}
